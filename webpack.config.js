module.exports = {
    entry: [
        'webpack-dev-server/client?http://localhost:3000',
        "./src/index.tsx"
    ],
    devtool: "source-map",
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist"
    },

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    },
    
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
};