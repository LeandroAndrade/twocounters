import * as React from "react";
import * as ReactDOM from "react-dom";
import { Counter } from "./components/counter"


class App extends React.Component<{}, IAppState> {
    constructor() {
        super();
        this.state = { counter: 0 };
    }

    incrementCounter = (): void => {
        this.setState((prevState) => ({
            counter: prevState.counter + 1
        }));
    }

    decrementCounter = (): void => {
        this.setState((prevState) => ({
            counter: prevState.counter > 0
                ? prevState.counter - 1
                : 0
        }));
    }

    render() {
        return (
            <div>
                <Counter onClickIncrement={this.incrementCounter} onClickDecrement={this.decrementCounter} counterTxt={this.state.counter} />
                <Counter onClickIncrement={this.incrementCounter} onClickDecrement={this.decrementCounter} counterTxt={this.state.counter} />
            </div>
        );
    }
}


ReactDOM.render(
    <App />,
    document.getElementById("counter")
);

