
interface IButtons{
    clickIncrement():void;
    clickDecrement():void;
}

interface IButtonsState{
}

interface ICounterTextProps{
    counterText: number;
}

interface ICounterProps{
    onClickIncrement():void;
    onClickDecrement():void;
    counterTxt:number;
}

interface ICounterState{}

interface IAppState{
    counter:number;
}