import * as React from "react";

class CounterButtons extends React.Component<IButtons, IButtonsState> {
    render() {
        return (
            <div>
                <button className="waves-effect waves-light btn green" onClick={this.props.clickIncrement}>
                    Increment
                </button>
                <button className="waves-effect waves-light btn red lighten-1" onClick={this.props.clickDecrement}>
                    Decrement
                </button>
            </div>
        );
    }
}

const CounterText = (props: ICounterTextProps) => {
	return (
        <h1>{props.counterText}</h1>
    );
}

export class Counter extends React.Component<ICounterProps, ICounterState>{
    render() {
        return (
            <div>
                <CounterText counterText={this.props.counterTxt} />
                <CounterButtons clickIncrement={this.props.onClickIncrement} clickDecrement={this.props.onClickDecrement} />
            </div>
        );
    }
}
